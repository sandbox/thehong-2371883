<?php
/**
 * @file
 * linkhay.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function linkhay_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'user_view_panel_context';
  $handler->task = 'user_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '93f930f2-c250-47c7-97f8-4d02403a0f17';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2e5cda3b-6a5c-46cc-a3fc-1b421003c389';
    $pane->panel = 'middle';
    $pane->type = 'user_picture';
    $pane->subtype = 'user_picture';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_entity_id:user_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2e5cda3b-6a5c-46cc-a3fc-1b421003c389';
    $display->content['new-2e5cda3b-6a5c-46cc-a3fc-1b421003c389'] = $pane;
    $display->panels['middle'][0] = 'new-2e5cda3b-6a5c-46cc-a3fc-1b421003c389';
    $pane = new stdClass();
    $pane->pid = 'new-9e5cf1f9-b9f0-4dc9-bb52-3da9a35423d1';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'quicktabs-member_contribution';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Contributions',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9e5cf1f9-b9f0-4dc9-bb52-3da9a35423d1';
    $display->content['new-9e5cf1f9-b9f0-4dc9-bb52-3da9a35423d1'] = $pane;
    $display->panels['middle'][1] = 'new-9e5cf1f9-b9f0-4dc9-bb52-3da9a35423d1';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-2e5cda3b-6a5c-46cc-a3fc-1b421003c389';
  $handler->conf['display'] = $display;
  $export['user_view_panel_context'] = $handler;

  return $export;
}
