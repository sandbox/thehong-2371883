<?php
/**
 * @file
 * linkhay.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function linkhay_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'member_contribution';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Member\'s contribution';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'user_comment',
      'display' => 'default',
      'args' => '',
      'title' => 'Comments',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'user_s_posts',
      'display' => 'default',
      'args' => '',
      'title' => 'Submitted Posts',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Comments');
  t('Member\'s contribution');
  t('Submitted Posts');

  $export['member_contribution'] = $quicktabs;

  return $export;
}
